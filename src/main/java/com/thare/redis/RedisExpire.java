package com.thare.redis;

import java.util.concurrent.TimeUnit;

public class RedisExpire {

    public final static long VERIFY_CODE_TIME = 5;

    public final static TimeUnit VERIFY_CODE_UNIT = TimeUnit.MINUTES;

}
