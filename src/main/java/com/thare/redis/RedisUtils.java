package com.thare.redis;

import com.thare.common.RandomString;
import org.springframework.data.redis.core.ValueOperations;

public class RedisUtils {

    private static ValueOperations<String, String> valueOperations;

    public static String setVerifyCode(String email) {
        String key = RedisKey.getVerifyCodeKey(email);
        String value = RandomString.getVerifyCode();
        valueOperations.set(key, value, RedisExpire.VERIFY_CODE_TIME, RedisExpire.VERIFY_CODE_UNIT);
        return value;
    }

    public static String getVerifyCode(String email) {
        String key = RedisKey.getVerifyCodeKey(email);
        String code = valueOperations.get(key);
        return code;
    }

    public ValueOperations<String, String> getValueOperations() {
        return valueOperations;
    }

    public void setValueOperations(ValueOperations<String, String> valueOperations) {
        RedisUtils.valueOperations = valueOperations;
    }
}
