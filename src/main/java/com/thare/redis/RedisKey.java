package com.thare.redis;

public class RedisKey {

    private final static String HEAD = "redp";

    private final static String VERIFY_CODE = "verifycode";

    public static String getVerifyCodeKey(String email) {
        return String.format("%s:%s:%s", HEAD, VERIFY_CODE, email);
    }
}
