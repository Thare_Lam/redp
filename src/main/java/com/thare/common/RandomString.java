package com.thare.common;

import java.util.Random;

public class RandomString {

    private final static String STR = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    private final static int LEN = STR.length();

    private final static int VERIFY_CODE_LENGTH = 4;

    private static String getRandomString(int len) {
        char[] s = new char[len];
        Random random = new Random();
        for (int i = 0; i < len; ++i) {
            s[i] = STR.charAt(random.nextInt(LEN));
        }
        return String.valueOf(s);
    }

    public static String getVerifyCode() {
        return getRandomString(VERIFY_CODE_LENGTH);
    }

}
