package com.thare.common;

import com.alibaba.fastjson.JSON;

public class JsonUtils {

    public static String toJson(Object object) {
        return JSON.toJSONString(object);
    }

}
