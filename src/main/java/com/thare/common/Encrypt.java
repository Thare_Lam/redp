package com.thare.common;

import java.math.BigInteger;
import java.security.MessageDigest;

public class Encrypt {

    private final static String SALT = "bGRpZmhr";

    public static String encryptPassword(String password) {
        return calMD5(password + SALT);
    }

    private static String calMD5(String str) {
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(str.getBytes("UTF-8"));
            BigInteger bg = new BigInteger(1, md5.digest());
            return bg.toString(16);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }

}
