package com.thare.common;

import java.util.HashMap;
import java.util.Map;

public class Utils {

    public static boolean isNullOrEmpty(String str) {
        return str == null || "".equals(str);
    }

    public static Map<String, Object> toSimpleMap(boolean status) {
        Map<String, Object> map = new HashMap<>();
        map.put("status", status);
        return map;
    }

    public static Map<String, Object> toSimpleMap(String key, boolean status) {
        Map<String, Object> map = new HashMap<>();
        map.put(key, status);
        return map;
    }

    public static String simpleResponse(boolean status) {
        Map result = toSimpleMap(status);
        return JsonUtils.toJson(result);
    }

}
