package com.thare.common;

import com.thare.vo.User;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginManager {

    public static User getUserFromRequest(HttpServletRequest request) {
        return (User)request.getSession().getAttribute(ConstantUtils.USER);
    }

    public static void setCookie(HttpServletRequest request, User user) {
        HttpSession session = request.getSession();
        session.setAttribute(ConstantUtils.USER, user);
    }

    public static boolean logout(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.removeAttribute(ConstantUtils.USER);
        return true;
    }

    public static int getLoginStatus(HttpServletRequest request) {
        User user = getUserFromRequest(request);
        if (user == null) {
            return 0;
        }
        if (user.getStatus() == 1) {
            return 1;
        } else {
            return 2;
        }
    }

    public static void setLoginFail(HttpServletResponse response) {
        response.setStatus(HttpStatusCode.FORBIDDEN);
    }

    public static void needVerify(HttpServletResponse response) {
        response.setStatus(HttpStatusCode.UNAUTHORIZED);
    }

}
