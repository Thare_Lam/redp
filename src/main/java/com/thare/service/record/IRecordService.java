package com.thare.service.record;

import com.thare.vo.Record;

public interface IRecordService {

    public boolean add(Record record);

    public Record[] getRecords(Integer userId);

}
