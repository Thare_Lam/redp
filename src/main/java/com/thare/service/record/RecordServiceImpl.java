package com.thare.service.record;

import com.thare.dao.IRecordDao;
import com.thare.vo.Record;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RecordServiceImpl implements IRecordService {

    @Autowired
    private IRecordDao recordDao;

    @Override
    public boolean add(Record record) {
        Record existRecord = recordDao.findByUserIdAndName(record.getUserId(), record.getName());
        return existRecord == null && recordDao.doCreate(record) > 0;
    }

    @Override
    public Record[] getRecords(Integer userId) {
        return recordDao.findByUserId(userId);
    }
}
