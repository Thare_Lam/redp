package com.thare.service.user;

import com.thare.vo.User;

import java.util.Map;

public interface IUserService {

    public boolean add(User user);

    public void update(User user);

    public User getById(Integer id);

    public Map<String, Object> check(String email, String password);

}
