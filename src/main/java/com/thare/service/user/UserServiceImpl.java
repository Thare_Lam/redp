package com.thare.service.user;

import com.thare.common.ConstantUtils;
import com.thare.common.Utils;
import com.thare.dao.IUserDao;
import com.thare.vo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private IUserDao userDao;

    @Override
    public boolean add(User user) {
        User existUser = userDao.findByEmailOrName(user.getEmail(), user.getName());
        return existUser == null && userDao.doCreate(user) > 0;
    }

    @Override
    public void update(User user) {
        userDao.doUpdate(user);
    }

    @Override
    public User getById(Integer id) {
        return userDao.findById(id);
    }

    @Override
    public Map<String, Object> check(String email, String password) {
        Map<String, Object> map = Utils.toSimpleMap(false);
        User user = userDao.findByEmailAndPassword(email, password);
        if (user != null) {
            map.put(ConstantUtils.STATUS, true);
            map.put(ConstantUtils.USER, user);
        }
        return map;
    }
}
