package com.thare.dao;

import com.thare.vo.Record;
import org.springframework.stereotype.Repository;

@Repository
public interface IRecordDao {

    public int doCreate(Record user);

    public Record[] findByUserId(Integer userId);

    public Record findByUserIdAndName(Integer userId, String name);

}
