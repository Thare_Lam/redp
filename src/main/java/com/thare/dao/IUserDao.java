package com.thare.dao;

import com.thare.vo.User;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserDao {

    public int doCreate(User user);

    public void doUpdate(User user);

    public User findById(Integer id);

    public User findByEmailOrName(String email, String name);

    public User findByEmailAndPassword(String email, String password);

}
