package com.thare.message;

import com.thare.common.JsonUtils;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class MessageSender {

    @Autowired
    private AmqpTemplate amqpTemplate;

    public void send(Object message) {
        amqpTemplate.convertAndSend(message);
    }

}
