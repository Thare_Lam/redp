package com.thare.controller;

import com.thare.common.ConstantUtils;
import com.thare.common.JsonUtils;
import com.thare.common.LoginManager;
import com.thare.common.Utils;
import com.thare.service.record.IRecordService;
import com.thare.vo.Record;
import com.thare.vo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@RequestMapping("/record/*")
public class RecordController {

    @Autowired
    private IRecordService recordService;

    @ResponseBody
    @RequestMapping(value = "list.do", method = RequestMethod.POST)
    public String list(HttpServletRequest request) {
        User user = LoginManager.getUserFromRequest(request);
        Record[] recordList = recordService.getRecords(user.getId());
        Map<String, Object> map = Utils.toSimpleMap(true);
        map.put(ConstantUtils.DATA, recordList);
        return JsonUtils.toJson(map);
    }

    @ResponseBody
    @RequestMapping("add.do")
    public String add(HttpServletRequest request, Record record) {
        User user = LoginManager.getUserFromRequest(request);
        record.setUserId(user.getId());
        boolean addSuccess = recordService.add(record);
        return Utils.simpleResponse(addSuccess);
    }

}
