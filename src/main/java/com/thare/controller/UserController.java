package com.thare.controller;

import com.thare.common.*;
import com.thare.message.MessageSender;
import com.thare.redis.RedisUtils;
import com.thare.service.user.IUserService;
import com.thare.vo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/user/*")
public class UserController {

    @Autowired
    private MessageSender messageSender;

    @Autowired
    private IUserService userService;

    @ResponseBody
    @RequestMapping(value = "register", method = RequestMethod.POST)
    public String register(HttpServletRequest request, User user) {
        sendVerifyCode(user);
        preHandle(user);
        boolean status = userService.add(user);
        if (status) {
            LoginManager.setCookie(request, user);
        }
        return Utils.simpleResponse(status);
    }

    @ResponseBody
    @RequestMapping(value = "verify", method = RequestMethod.POST)
    public String verify(HttpServletRequest request, String code) {
        boolean status = verifyCode(request, code);
        if (status) {
            updateUserStatus(request);
        }
        return Utils.simpleResponse(status);
    }

    @ResponseBody
    @RequestMapping(value = "login", method = RequestMethod.POST)
    public String login(HttpServletRequest request, String email, String password) {
        password = Encrypt.encryptPassword(password);
        Map<String, Object> map = userService.check(email, password);
        boolean loginSuccess = (boolean)map.get(ConstantUtils.STATUS);
        if (loginSuccess) {
            User user = (User) map.get(ConstantUtils.USER);
            LoginManager.setCookie(request, user);
        }
        return Utils.simpleResponse(loginSuccess);
    }

    @ResponseBody
    @RequestMapping(value = "logout", method = RequestMethod.POST)
    public String logout(HttpServletRequest request) {
        boolean logoutSuccess = LoginManager.logout(request);
        return Utils.simpleResponse(logoutSuccess);
    }

    @ResponseBody
    @RequestMapping(value = "checkLogin", method = RequestMethod.POST)
    public String checkLogin(HttpServletRequest request) {
        boolean isLogin = LoginManager.getUserFromRequest(request) != null;
        return Utils.simpleResponse(isLogin);
    }

    private void preHandle(User user) {
        String newPassword = Encrypt.encryptPassword(user.getPassword());
        user.setPassword(newPassword);
        user.setStatus(0);
    }

    private void sendVerifyCode(User user) {
        Map<String, String> map = new HashMap<>();
        String email = user.getEmail();
        String name = user.getName();
        String code = RedisUtils.setVerifyCode(email);
        map.put(MQConstantUtils.VERIFY_EMAIL, email);
        map.put(MQConstantUtils.VERIFY_NAME, name);
        map.put(MQConstantUtils.VERIFY_CODE, code);
        String msg = JsonUtils.toJson(map);
        messageSender.send(msg);
    }

    private void updateUserStatus(HttpServletRequest request) {
        User user = LoginManager.getUserFromRequest(request);
        Integer userId = user.getId();
        updateDatabaseUserStatus(userId);
        updateSessionUserStatus(request, userId);
    }

    private void updateDatabaseUserStatus(Integer userId) {
        User updateUser = new User();
        updateUser.setId(userId);
        updateUser.setStatus(1);
        userService.update(updateUser);
    }

    private void updateSessionUserStatus(HttpServletRequest request, Integer userId) {
        User user = userService.getById(userId);
        LoginManager.setCookie(request, user);
    }

    public static boolean verifyCode(HttpServletRequest request, String code) {
        User user = LoginManager.getUserFromRequest(request);
        if (user == null) {
            return false;
        }
        String correctCode = RedisUtils.getVerifyCode(user.getEmail());
        if (correctCode == null) {
            return false;
        }
        return correctCode.equals(code);
    }

}
