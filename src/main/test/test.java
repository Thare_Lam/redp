import com.thare.common.RandomString;
import com.thare.redis.RedisExpire;
import com.thare.redis.RedisKey;
import com.thare.redis.RedisUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import sun.misc.BASE64Encoder;

import javax.annotation.Resource;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;

@RunWith(SpringJUnit4ClassRunner.class)  //使用junit4进行测试
@ContextConfiguration({"/applicationContext.xml"}) //加载配置文件
public class test {

    @Resource(name = "redisTemplate")
    private ValueOperations<String, String> valueOperations;

    public static String calSalt(String str) {
        BASE64Encoder encoder = new BASE64Encoder();
        String encode = encoder.encode(str.getBytes());
        return encode;
    }

    @Test
    public void randomString() {
        setVerifyCode("abc");
        System.out.println(
        getVerifyCode("abc"));
    }



    private String setVerifyCode(String email) {
        String key = RedisKey.getVerifyCodeKey(email);
        String value = RandomString.getVerifyCode();
        valueOperations.set(key, value, RedisExpire.VERIFY_CODE_TIME, RedisExpire.VERIFY_CODE_UNIT);
        return value;
    }

    private String getVerifyCode(String email) {
        String key = RedisKey.getVerifyCodeKey(email);
        String code = valueOperations.get(key);
        return code;
    }

}
